

/*
	To be able to create routes from another file to be used



	the Router() method will us to container our routes
*/


const express = require("express");
const router = express.Router();

//All route to courses now has an endpoint prefaced with /courses 
//endpoint - /courses/courses

const courseControllers = require("../controllers/courseControllers");



// import the  route 
const auth = require("../auth")
const {verify,verifyAdmin} = auth;


router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);
//endpoint /courses/
//only logged is user is able to use addCourse
//
	router.post('/',verify,verifyAdmin,courseControllers.addCourse);

    // get all active course - (regular, non -loggeg in)
    router.get('/activeCourses', courseControllers.getActiveCourses)

	// impport the route of details
	router.post("/getSingleCourse", courseControllers.getSingleCourse);

module.exports = router;