

/*
	Naming convention for controllers is that it should be named after the model/documents it is concerned with

	Controllers are functions which contain the actual business logic of our API and is triggered by a route

	MVC - models views and controller
*/

//To create a controller, we first add it into our module.exports
// So that we can import the controllers from our module

// import the user model in the controllers instead because this where are now going to use it
const User = require("../models/User");
//Import bcrypt
//bcrypt is a package which allows us to hash our passwords to add a layer of security for our's 
const bcrypt = require("bcrypt");

// import auth.js module to create createAccesToken  and its subsequent metthods

const auth = require("../auth");

module.exports.registerUser = (req,res)=> {

	//console.log(req.body) //check the input passed via the client
	// using bcrypt , hide the users password underneath the a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

	//bcrypt 
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email:req.body.email,
		password:hashedPw,
		mobileNo: req.body.mobileNo
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// getUserDetails should only allow the LOGGED-in user to grt his OWN details
module.exports.getUserDetails = (req,res)=>{
	// console.log(req.user)// contains the deatails of the looged in user
	//find() will return an array of documents which matches the criteria
	// It will return an array even if it only found 1 document.
	//User.findOne({_id:req.body.id})

	//findOne() will return a single document that matchd our criteria
	//User.findOne({_id:req.body.id})

	//findById() is a mongoose method that will allow us to find a document strictly by its id
	User.findById(req.body.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{
	console.log(req.body);


/*
	steps for login in our user session
	1. if we found the user, we will  check his password  if the passwrod
*/


// 
User.findOne({email:req.body.email})
.then(foundUser =>{
		

	if(foundUser === null){
		return res.send({message: "No user Found!"});
	}else{
		// console.log(foundUser); if we fin
	}
	
	const isPasswordCorrect = bcrypt.compareSync(req.body.password,
		foundUser.password)

	// console.log(isPasswordCorrect)
	
	if(isPasswordCorrect){
			/**
			 * to be able create a "key " or token that allows/authorize
			 * 
			 */	
				// 
				return res.send({accessToken: auth.createAccesToken(foundUser)});

			// console.log("we will create a token for the user if the password is correct")
	}else{
			return res.send({message:"Incorrect password"})
	}
})

}

module.exports.getcheckEmail = (req,res) => {
	
	User.find({email:req.body.email})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}
