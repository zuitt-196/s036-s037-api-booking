
//Import the course model so we can manipultae it and add a new course document.
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{
	//Use the course model to connect to our collection and retrieve our courses
	// To be able to query into our colletions we use the model connected to that collection
	// in mongodb db.course.find({})
	//Model.find()-returns a colelction of documents that matches our criteria similar to mongodb's find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.addCourse = (req,res) => {

		//console.log(req.body);
		// res.send("This route will create a new course documents");

		//Using the course model, we will use its constructor to create our course document which will follow the schema of the model, and add methods for documents creation
		let newCourse = new Course({

			name:req.body.name,
			description: req.body.description,
			price: req.body.price
		})

		// console.log(newCourse);
		//newCourse is now an object which follows the courseScheme and with additional methods from our course constructor

		//.save() method is added into our newCourse. This will allow us to save the content of our newCOurse into out collection

		//.then() allows us to process the result of a previous function/method in its own anonymous

		//.catch() - catches the errors and allows 

		newCourse.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
	}



module.exports.getActiveCourses = (req, res) => {
	// find all active Course mod
	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}




// const auth = require("../auth");
module.exports.getSingleCourse = (req,res) => {

	Course.findById(req.body.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}