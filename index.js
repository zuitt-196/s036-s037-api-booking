// create an express API server
const express = require('express');
// monggoes is an ODm library to let our Express APi manipulate a mongoDb database
const mongoose = require("mongoose");
const app = express();
const port = 4000;

// app.get('/user', (req, res) => {

// })

// 
 // mongoes connection
    /* 
        mongoes.connect () is a methpod to connetct api with oue mongodb database via the use of mongoose. it has 2 arguments. first, is the connection string to connect our api to pur mongodb, second, is an object used to add ionformation betwee mongoes and mongodb
    
    */

 mongoose.connect("mongodb+srv://admin:admin123@cluster0.uctt7.mongodb.net/bookingAPI?retryWrites=true&w=majority",
 {
    useNewUrlParser: true,
    useUnifiedTopology: true


 });       

 //we will create notications  if the the connection to the db is succes or failled.

 let db = mongoose.connection;

 // this is to show notification of an internal server wrror from mongoDb
 db.once('error',console.error.bind(console, "MongoDB Connection Error,"))
 // if the connection is open and successfully, we will output a message in terminal/gitbash
db.once('open',()=>console.log("Connected to MongoDB"))

// expres.json() to be able to handel to request body anb parse into Js object
app.use(express.json())


// import our routes and use it as middleware
//which means,that we will be able  to group  together our routes and middle

const courseRoutes = require('./routes/courseRoutes')
app.use('/courses',courseRoutes)

//import the user routes
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);




app.listen(port,()=> console.log("Express API running at localhost 4000!"));
