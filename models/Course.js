

// the naming convinction fir model files is the singular and capitalized

const mongoose = require("mongoose");


//  Moogoose Schemas


const courseSchema = new mongoose.Schema({


    name: {
        type:String,
        required: [true,"Name is required"]
    },
    description: {
        type: String,
        required: [true,"Description is required"]
    },
    price: {
        type: Number,
        required: [true,"Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    enrollees: [

        {
            userId: {
                type: String,
                required: [true,"User Id is required"]
            },
            dateEnrolled: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }

    ]

})


// modules.exports - so we can inport and use this file in anothe file

// mongoose Model

module.exports = mongoose.model("Course",courseSchema);